<!--
 * @Author: Klien
 * @Date: 2022-05-10 11:35:55
 * @LastEditTime: 2022-05-10 11:37:11
 * @LastEditors: Klien
-->
# Tools-Cli

Node 自动化工具

## 本地运行测试。
    1.yarn build
    2.npm link / yarn link
    3.tools 命令

## 发布到npm私有源,私有源地址:http://34.92.114.30:4873/

## 发布步骤
   1.npm adduser --registry http://34.92.114.30:4873/

   2.npm publish --registry http://34.92.114.30:4873/

## 发布到npm私有源之后使用
    1.npm config ls
    2.npm set registry http://34.92.114.30:4873/
    3.npm intall tools-cli -g 
    4.
      - [ ] tools init           ------初始化
      - [ ] tools imagesmin 地址  ----压缩图片
      - [ ] 后续如果有需要,可以添加很多命令来协助
## 将图片无损压缩改为有损压缩
