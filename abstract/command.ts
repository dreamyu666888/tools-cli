import { AbstractAction } from './action';

import { props } from '../types';

import { Command } from 'commander';

export abstract class AbstractCommand {
	constructor(protected action: AbstractAction) {}

	public abstract load<T extends props<T>>(program: Command): void;
}
