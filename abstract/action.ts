import {props} from "../types";

export abstract class AbstractAction {
    public abstract initAction<T extends props<T>>(input?: string[]): Promise<void>
}