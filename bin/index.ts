#!/usr/bin/env node

/*
 * @Author: Klien
 * @Date: 2022-05-07 22:11:49
 * @LastEditTime: 2022-05-07 23:18:29
 * @LastEditors: Klien
 */

import { Command } from 'commander';

import { CommandLoader } from '../loader/commands';

const bootStrap = () => {
	const program: Command = new Command();

	program.version("1.0.0");

	CommandLoader.load<string>('init', program);

	CommandLoader.load<string>('imagesmin', program);

	program.parse(process.argv);

	if (!program.args.length) {
		program.outputHelp();
	}
};

bootStrap();
