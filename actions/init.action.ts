/*
 * @Author: Klien
 * @Date: 2022-05-07 22:14:55
 * @LastEditTime: 2022-05-07 23:09:39
 * @LastEditors: Klien
 */
import chalk from 'chalk';

import figlet from 'figlet';

import { AbstractAction } from '../abstract/action';

export class InitAction extends AbstractAction {
	public async initAction() {
		console.log(
			chalk.green(
				figlet.textSync('init Success', {
					font: 'Ghost',
					horizontalLayout: 'default',
					verticalLayout: 'default',
				})
			)
		);
	}
}
