/*
 * @Author: Klien
 * @Date: 2022-05-07 22:13:32
 * @LastEditTime: 2022-05-07 23:23:48
 * @LastEditors: Klien
 */
import { Command } from 'commander';

import { InitAction,ImagesMinAction } from '../../actions';

import { InitCommand, ImagesMinCommand } from '../../commands';

import { props } from '../../types';

export class CommandLoader {
	public static load<T extends props<T>>(name: string, program: Command): void {
		switch (name) {
			case 'init':
				new InitCommand(new InitAction()).load(program);
				break;
			case 'imagesmin':
				new ImagesMinCommand(new ImagesMinAction()).load(program);
				break;
			default:
				break;
		}
	}
}
