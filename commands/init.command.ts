/*
 * @Author: Klien
 * @Date: 2022-05-07 22:14:50
 * @LastEditTime: 2022-05-08 11:02:07
 * @LastEditors: Klien
 */
import { Command } from 'commander';

import { AbstractCommand } from '../abstract/command';

export class InitCommand extends AbstractCommand {
	public load(program: Command) {
		program
			.command('init')
			.alias('i')
			.description('初始化')
			.action(async () => {
				await this.action.initAction<string[]>()
			});
	}
}
